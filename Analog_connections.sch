EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "MightyPix Run2020 PCB"
Date "2021-02-08"
Rev "1.0"
Comp "HISKP. University of Bonn."
Comment1 "Design by: Klaas Padeken"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Jumper JP?
U 1 1 615BA146
P 1700 1000
AR Path="/615BA146" Ref="JP?"  Part="1" 
AR Path="/61541A83/615BA146" Ref="JP19"  Part="1" 
F 0 "JP19" H 1700 775 50  0000 C BNN
F 1 "J_AmpOutTEST" H 1700 866 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1700 1000 50  0001 C CNN
F 3 "~" H 1700 1000 50  0001 C CNN
	1    1700 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1000 2000 1000
$Comp
L power:GND #PWR?
U 1 1 615BA14D
P 2050 1300
AR Path="/606EC308/615BA14D" Ref="#PWR?"  Part="1" 
AR Path="/5FA940BE/615BA14D" Ref="#PWR?"  Part="1" 
AR Path="/615BA14D" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/615BA14D" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 2050 1050 50  0001 C CNN
F 1 "GND" H 2055 1127 50  0000 C CNN
F 2 "" H 2050 1300 50  0001 C CNN
F 3 "" H 2050 1300 50  0001 C CNN
	1    2050 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 615BA153
P 3600 1600
AR Path="/615BA153" Ref="R?"  Part="1" 
AR Path="/61541A83/615BA153" Ref="R19"  Part="1" 
F 0 "R19" V 3393 1600 50  0000 C CNN
F 1 "1k" V 3484 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3530 1600 50  0001 C CNN
F 3 "~" H 3600 1600 50  0001 C CNN
	1    3600 1600
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 615BA159
P 3900 1650
AR Path="/606EC308/615BA159" Ref="#PWR?"  Part="1" 
AR Path="/5FA940BE/615BA159" Ref="#PWR?"  Part="1" 
AR Path="/615BA159" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/615BA159" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 3900 1400 50  0001 C CNN
F 1 "GND" H 3905 1477 50  0000 C CNN
F 2 "" H 3900 1650 50  0001 C CNN
F 3 "" H 3900 1650 50  0001 C CNN
	1    3900 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 615BA15F
P 3100 1600
AR Path="/615BA15F" Ref="R?"  Part="1" 
AR Path="/61541A83/615BA15F" Ref="R18"  Part="1" 
F 0 "R18" V 2893 1600 50  0000 C CNN
F 1 "1k" V 2984 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3030 1600 50  0001 C CNN
F 3 "~" H 3100 1600 50  0001 C CNN
	1    3100 1600
	0    -1   1    0   
$EndComp
Wire Wire Line
	3350 1200 3350 1600
Wire Wire Line
	2250 1200 2250 1600
Wire Wire Line
	2250 1600 2950 1600
Wire Wire Line
	2250 1600 1450 1600
Connection ~ 2250 1600
Wire Wire Line
	2050 1300 2050 1100
Wire Wire Line
	2050 1100 2250 1100
Wire Wire Line
	3900 1650 3900 1600
Wire Wire Line
	3900 1600 3750 1600
Wire Wire Line
	3450 1600 3350 1600
Connection ~ 3350 1600
Wire Wire Line
	3350 1600 3250 1600
Text Label 1450 1600 0    50   ~ 0
FAST_ADC0
$Comp
L power:GND #PWR?
U 1 1 615BA178
P 950 1200
AR Path="/615BA178" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/615BA178" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 950 950 50  0001 C CNN
F 1 "GND" H 955 1027 50  0000 C CNN
F 2 "" H 950 1200 50  0001 C CNN
F 3 "" H 950 1200 50  0001 C CNN
	1    950  1200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 615BA17E
P 950 1000
AR Path="/615BA17E" Ref="J?"  Part="1" 
AR Path="/61541A83/615BA17E" Ref="J2"  Part="1" 
F 0 "J2" H 878 1238 50  0000 C CNN
F 1 "AmpOutTEST_Conn" H 878 1147 50  0000 C CNN
F 2 "dmaps_components:LEMO_connector" H 950 1000 50  0001 C CNN
F 3 " ~" H 950 1000 50  0001 C CNN
	1    950  1000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 1000 1150 1000
$Comp
L MightyPixRun2020_PCB-rescue:LMV721M5-SamacSys_Parts-LFMONOPIX2_PCB-rescue IC?
U 1 1 615BA18E
P 2250 1200
AR Path="/615BA18E" Ref="IC?"  Part="1" 
AR Path="/61541A83/615BA18E" Ref="IC1"  Part="1" 
F 0 "IC1" H 2800 735 50  0000 C CNN
F 1 "LMV721M5" H 2800 826 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 3200 1300 50  0001 L CNN
F 3 "https://www.digikey.com/product-detail/en/texas-instruments/LMV721M5-NOPB/LMV721M5-NOPBCT-ND/364401" H 3200 1200 50  0001 L CNN
F 4 "Operational Amplifier" H 3200 1100 50  0001 L CNN "Description"
F 5 "1.45" H 3200 1000 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 3200 900 50  0001 L CNN "Manufacturer_Name"
F 7 "LMV721M5" H 3200 800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "LMV721M5" H 3200 700 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/lmv721m5/texas-instruments" H 3200 600 50  0001 L CNN "Arrow Price/Stock"
F 10 "926-LMV721M5" H 3200 500 50  0001 L CNN "Mouser Part Number"
F 11 "https://www.mouser.com/Search/Refine.aspx?Keyword=926-LMV721M5" H 3200 400 50  0001 L CNN "Mouser Price/Stock"
	1    2250 1200
	1    0    0    1   
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 615BA194
P 3900 900
AR Path="/615BA194" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/615BA194" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 3900 750 50  0001 C CNN
F 1 "VCC" H 3917 1073 50  0000 C CNN
F 2 "" H 3900 900 50  0001 C CNN
F 3 "" H 3900 900 50  0001 C CNN
	1    3900 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 615BA19A
P 3900 1300
AR Path="/606EC308/615BA19A" Ref="#PWR?"  Part="1" 
AR Path="/5FA940BE/615BA19A" Ref="#PWR?"  Part="1" 
AR Path="/615BA19A" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/615BA19A" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 3900 1050 50  0001 C CNN
F 1 "GND" H 3905 1127 50  0000 C CNN
F 2 "" H 3900 1300 50  0001 C CNN
F 3 "" H 3900 1300 50  0001 C CNN
	1    3900 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 615BA1A0
P 3900 1150
AR Path="/606EC308/615BA1A0" Ref="C?"  Part="1" 
AR Path="/5FA940BE/615BA1A0" Ref="C?"  Part="1" 
AR Path="/615BA1A0" Ref="C?"  Part="1" 
AR Path="/61541A83/615BA1A0" Ref="C39"  Part="1" 
F 0 "C39" H 4015 1196 50  0000 L CNN
F 1 "100nF" H 4015 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 3938 1000 50  0001 C CNN
F 3 "~" H 3900 1150 50  0001 C CNN
	1    3900 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 900  3900 950 
Wire Wire Line
	3900 950  3350 950 
Wire Wire Line
	3350 950  3350 1100
Connection ~ 3900 950 
Wire Wire Line
	3900 950  3900 1000
Wire Wire Line
	1400 1000 1400 650 
Wire Wire Line
	1400 650  1050 650 
Connection ~ 1400 1000
Text HLabel 1050 650  0    50   Output ~ 0
AmpOut
$Comp
L Connector:Conn_Coaxial J?
U 1 1 6162C73A
P 1450 4350
AR Path="/6162C73A" Ref="J?"  Part="1" 
AR Path="/61541A83/6162C73A" Ref="J4"  Part="1" 
F 0 "J4" H 1378 4588 50  0000 C CNN
F 1 "PStop_Conn" H 1378 4497 50  0000 C CNN
F 2 "dmaps_components:LEMO_connector" H 1450 4350 50  0001 C CNN
F 3 " ~" H 1450 4350 50  0001 C CNN
	1    1450 4350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6162C740
P 1450 4750
AR Path="/6162C740" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/6162C740" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 1450 4500 50  0001 C CNN
F 1 "GND" H 1455 4577 50  0000 C CNN
F 2 "" H 1450 4750 50  0001 C CNN
F 3 "" H 1450 4750 50  0001 C CNN
	1    1450 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6162C746
P 2250 4350
AR Path="/6162C746" Ref="R?"  Part="1" 
AR Path="/61541A83/6162C746" Ref="R16"  Part="1" 
F 0 "R16" V 2043 4350 50  0000 C CNN
F 1 "10k" V 2134 4350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2180 4350 50  0001 C CNN
F 3 "~" H 2250 4350 50  0001 C CNN
	1    2250 4350
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 6162C74C
P 2450 4550
AR Path="/6162C74C" Ref="C?"  Part="1" 
AR Path="/61541A83/6162C74C" Ref="C35"  Part="1" 
F 0 "C35" H 2565 4596 50  0000 L CNN
F 1 "10nF(HV)" H 2565 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2488 4400 50  0001 C CNN
F 3 "~" H 2450 4550 50  0001 C CNN
	1    2450 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 4350 2450 4400
Wire Wire Line
	1450 4550 1450 4700
Wire Wire Line
	2450 4700 1450 4700
Text Label 1650 4350 0    50   ~ 0
PStop_in
Wire Wire Line
	2450 4350 2400 4350
Connection ~ 2450 4350
Wire Wire Line
	2450 4350 2850 4350
Wire Wire Line
	1650 4350 2100 4350
$Comp
L Device:Jumper JP?
U 1 1 6162C78C
P 1800 4000
AR Path="/6162C78C" Ref="JP?"  Part="1" 
AR Path="/61541A83/6162C78C" Ref="JP20"  Part="1" 
F 0 "JP20" H 1800 3775 50  0000 C BNN
F 1 "J_PStop" H 1800 3866 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1800 4000 50  0001 C CNN
F 3 "~" H 1800 4000 50  0001 C CNN
	1    1800 4000
	1    0    0    1   
$EndComp
Wire Wire Line
	950  4000 1500 4000
Text Label 950  4000 0    50   ~ 0
BackBias_in
Wire Wire Line
	2100 4000 2100 4350
Connection ~ 2100 4350
Wire Wire Line
	1450 4750 1450 4700
Connection ~ 1450 4700
$Comp
L Connector:Conn_Coaxial J?
U 1 1 6168AEA8
P 5000 1200
AR Path="/6168AEA8" Ref="J?"  Part="1" 
AR Path="/61541A83/6168AEA8" Ref="J6"  Part="1" 
F 0 "J6" H 4928 1438 50  0000 C CNN
F 1 "NwellRing_Conn" H 4928 1347 50  0000 C CNN
F 2 "dmaps_components:LEMO_connector" H 5000 1200 50  0001 C CNN
F 3 " ~" H 5000 1200 50  0001 C CNN
	1    5000 1200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6168AEAE
P 5000 1650
AR Path="/6168AEAE" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/6168AEAE" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 5000 1400 50  0001 C CNN
F 1 "GND" H 5005 1477 50  0000 C CNN
F 2 "" H 5000 1650 50  0001 C CNN
F 3 "" H 5000 1650 50  0001 C CNN
	1    5000 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6168AEB4
P 5800 1200
AR Path="/6168AEB4" Ref="R?"  Part="1" 
AR Path="/61541A83/6168AEB4" Ref="R21"  Part="1" 
F 0 "R21" V 5593 1200 50  0000 C CNN
F 1 "1k" V 5684 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5730 1200 50  0001 C CNN
F 3 "~" H 5800 1200 50  0001 C CNN
	1    5800 1200
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 6168AEBA
P 6000 1400
AR Path="/6168AEBA" Ref="C?"  Part="1" 
AR Path="/61541A83/6168AEBA" Ref="C40"  Part="1" 
F 0 "C40" H 6115 1446 50  0000 L CNN
F 1 "100nF" H 6115 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6038 1250 50  0001 C CNN
F 3 "~" H 6000 1400 50  0001 C CNN
	1    6000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1200 6000 1250
Wire Wire Line
	5000 1400 5000 1550
Wire Wire Line
	6000 1550 5000 1550
Connection ~ 5000 1550
Wire Wire Line
	5000 1550 5000 1650
Text Label 5200 1200 0    50   ~ 0
NwellRing_in
$Comp
L Device:Jumper JP?
U 1 1 6168AEC6
P 6750 1750
AR Path="/6168AEC6" Ref="JP?"  Part="1" 
AR Path="/61541A83/6168AEC6" Ref="JP23"  Part="1" 
F 0 "JP23" H 6750 1525 50  0000 C BNN
F 1 "J_NwellRing" H 6750 1616 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6750 1750 50  0001 C CNN
F 3 "~" H 6750 1750 50  0001 C CNN
	1    6750 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 1750 6450 1750
Wire Wire Line
	6000 1200 5950 1200
Wire Wire Line
	5200 1200 5650 1200
$Comp
L Device:C C?
U 1 1 6168AED0
P 6500 1400
AR Path="/6168AED0" Ref="C?"  Part="1" 
AR Path="/61541A83/6168AED0" Ref="C43"  Part="1" 
F 0 "C43" H 6615 1446 50  0000 L CNN
F 1 "47uF" H 6615 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 6538 1250 50  0001 C CNN
F 3 "~" H 6500 1400 50  0001 C CNN
	1    6500 1400
	1    0    0    -1  
$EndComp
Connection ~ 6000 1550
Wire Wire Line
	6000 1550 6500 1550
$Comp
L Device:C C?
U 1 1 6168AED9
P 7000 1400
AR Path="/6168AED9" Ref="C?"  Part="1" 
AR Path="/61541A83/6168AED9" Ref="C44"  Part="1" 
F 0 "C44" H 7115 1446 50  0000 L CNN
F 1 "1uF" H 7115 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 7038 1250 50  0001 C CNN
F 3 "~" H 7000 1400 50  0001 C CNN
	1    7000 1400
	1    0    0    -1  
$EndComp
Connection ~ 6000 1200
Wire Wire Line
	6500 1250 6500 1200
Connection ~ 6500 1200
Wire Wire Line
	6500 1200 6000 1200
Wire Wire Line
	7000 1250 7000 1200
Connection ~ 7000 1200
Wire Wire Line
	7000 1200 6500 1200
Wire Wire Line
	7000 1550 6500 1550
Connection ~ 6500 1550
Wire Wire Line
	7500 1750 7050 1750
Wire Wire Line
	7000 1200 7500 1200
Wire Wire Line
	7500 1750 7500 1200
Connection ~ 7500 1200
Wire Wire Line
	7500 1200 7700 1200
$Comp
L Connector:Conn_Coaxial J?
U 1 1 616D735D
P 1500 5850
AR Path="/616D735D" Ref="J?"  Part="1" 
AR Path="/61541A83/616D735D" Ref="J5"  Part="1" 
F 0 "J5" H 1428 6088 50  0000 C CNN
F 1 "PwellRing_Conn" H 1428 5997 50  0000 C CNN
F 2 "dmaps_components:LEMO_connector" H 1500 5850 50  0001 C CNN
F 3 " ~" H 1500 5850 50  0001 C CNN
	1    1500 5850
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 616D7363
P 1500 6250
AR Path="/616D7363" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/616D7363" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 1500 6000 50  0001 C CNN
F 1 "GND" H 1505 6077 50  0000 C CNN
F 2 "" H 1500 6250 50  0001 C CNN
F 3 "" H 1500 6250 50  0001 C CNN
	1    1500 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 616D7369
P 2300 5850
AR Path="/616D7369" Ref="R?"  Part="1" 
AR Path="/61541A83/616D7369" Ref="R17"  Part="1" 
F 0 "R17" V 2093 5850 50  0000 C CNN
F 1 "10k" V 2184 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2230 5850 50  0001 C CNN
F 3 "~" H 2300 5850 50  0001 C CNN
	1    2300 5850
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 616D736F
P 2500 6050
AR Path="/616D736F" Ref="C?"  Part="1" 
AR Path="/61541A83/616D736F" Ref="C36"  Part="1" 
F 0 "C36" H 2615 6096 50  0000 L CNN
F 1 "10nF(HV)" H 2615 6005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2538 5900 50  0001 C CNN
F 3 "~" H 2500 6050 50  0001 C CNN
	1    2500 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 5850 2500 5900
Wire Wire Line
	1500 6050 1500 6200
Wire Wire Line
	2500 6200 1500 6200
Text Label 1700 5850 0    50   ~ 0
PwellRing_in
Wire Wire Line
	1000 5500 1550 5500
Text Label 1000 5500 0    50   ~ 0
BackBias_in
Wire Wire Line
	2500 5850 2450 5850
Connection ~ 2500 5850
Wire Wire Line
	1700 5850 2150 5850
Wire Wire Line
	2500 5850 3250 5850
Wire Wire Line
	2150 5500 2150 5850
Connection ~ 2150 5850
Wire Wire Line
	1500 6250 1500 6200
Connection ~ 1500 6200
$Comp
L Device:Jumper JP?
U 1 1 616D73D6
P 1850 5500
AR Path="/616D73D6" Ref="JP?"  Part="1" 
AR Path="/61541A83/616D73D6" Ref="JP21"  Part="1" 
F 0 "JP21" H 1850 5275 50  0000 C BNN
F 1 "J_PwellRing" H 1850 5366 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1850 5500 50  0001 C CNN
F 3 "~" H 1850 5500 50  0001 C CNN
	1    1850 5500
	1    0    0    1   
$EndComp
Text HLabel 2850 4350 2    50   Output ~ 0
PStop
Text HLabel 7700 1200 2    50   Output ~ 0
NwellRing
Text HLabel 3250 5850 2    50   Output ~ 0
PwellRing
Text HLabel 6850 2500 2    50   Output ~ 0
NoToVCO
$Comp
L power:GND #PWR071
U 1 1 61C1A322
P 5400 2500
F 0 "#PWR071" H 5400 2250 50  0001 C CNN
F 1 "GND" V 5405 2372 50  0000 R CNN
F 2 "" H 5400 2500 50  0001 C CNN
F 3 "" H 5400 2500 50  0001 C CNN
	1    5400 2500
	0    1    1    0   
$EndComp
$Comp
L Device:C C41
U 1 1 61C23929
P 6400 2500
F 0 "C41" V 6652 2500 50  0000 C CNN
F 1 "1nF" V 6561 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6438 2350 50  0001 C CNN
F 3 "~" H 6400 2500 50  0001 C CNN
	1    6400 2500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R20
U 1 1 61C24B3F
P 5750 2500
F 0 "R20" V 5543 2500 50  0000 C CNN
F 1 "1k" V 5634 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5680 2500 50  0001 C CNN
F 3 "~" H 5750 2500 50  0001 C CNN
	1    5750 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 2500 6250 2500
Wire Wire Line
	6550 2500 6650 2500
Wire Wire Line
	5400 2500 5500 2500
Wire Wire Line
	6650 2500 6650 2900
Wire Wire Line
	6650 2900 6300 2900
Wire Wire Line
	5500 2900 5500 2500
Connection ~ 6650 2500
Wire Wire Line
	6650 2500 6850 2500
Connection ~ 5500 2500
Wire Wire Line
	5500 2500 5600 2500
$Comp
L Connector:TestPoint_2Pole TP30
U 1 1 61C49D09
P 6100 2900
F 0 "TP30" H 6100 3095 50  0000 C CNN
F 1 "TestPoint_NoToVCO" H 6100 3004 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 6100 2900 50  0001 C CNN
F 3 "~" H 6100 2900 50  0001 C CNN
	1    6100 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2900 5500 2900
Text HLabel 6900 3500 2    50   Output ~ 0
ToVCO
$Comp
L power:GND #PWR072
U 1 1 61C5293A
P 5450 3500
F 0 "#PWR072" H 5450 3250 50  0001 C CNN
F 1 "GND" V 5455 3372 50  0000 R CNN
F 2 "" H 5450 3500 50  0001 C CNN
F 3 "" H 5450 3500 50  0001 C CNN
	1    5450 3500
	0    1    1    0   
$EndComp
$Comp
L Device:C C42
U 1 1 61C52940
P 6450 3500
F 0 "C42" V 6702 3500 50  0000 C CNN
F 1 "1nF" V 6611 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.08x0.95mm_HandSolder" H 6488 3350 50  0001 C CNN
F 3 "~" H 6450 3500 50  0001 C CNN
	1    6450 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R22
U 1 1 61C52946
P 5800 3500
F 0 "R22" V 5593 3500 50  0000 C CNN
F 1 "1k" V 5684 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5730 3500 50  0001 C CNN
F 3 "~" H 5800 3500 50  0001 C CNN
	1    5800 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	5950 3500 6300 3500
Wire Wire Line
	6600 3500 6700 3500
Wire Wire Line
	5450 3500 5550 3500
Wire Wire Line
	6700 3500 6700 3900
Wire Wire Line
	6700 3900 6350 3900
Wire Wire Line
	5550 3900 5550 3500
Connection ~ 6700 3500
Wire Wire Line
	6700 3500 6900 3500
Connection ~ 5550 3500
Wire Wire Line
	5550 3500 5650 3500
$Comp
L Connector:TestPoint_2Pole TP31
U 1 1 61C52956
P 6150 3900
F 0 "TP31" H 6150 4095 50  0000 C CNN
F 1 "TestPoint_ToVCO" H 6150 4004 50  0000 C CNN
F 2 "TestPoint:TestPoint_2Pads_Pitch2.54mm_Drill0.8mm" H 6150 3900 50  0001 C CNN
F 3 "~" H 6150 3900 50  0001 C CNN
	1    6150 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3900 5550 3900
$Comp
L Connector:Conn_Coaxial J?
U 1 1 6033A057
P 1400 2950
AR Path="/6033A057" Ref="J?"  Part="1" 
AR Path="/61541A83/6033A057" Ref="J3"  Part="1" 
F 0 "J3" H 1328 3188 50  0000 C CNN
F 1 "BackBias_Conn" H 1328 3097 50  0000 C CNN
F 2 "dmaps_components:LEMO_connector" H 1400 2950 50  0001 C CNN
F 3 " ~" H 1400 2950 50  0001 C CNN
	1    1400 2950
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6033A05D
P 1400 3350
AR Path="/6033A05D" Ref="#PWR?"  Part="1" 
AR Path="/61541A83/6033A05D" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 1400 3100 50  0001 C CNN
F 1 "GND" H 1405 3177 50  0000 C CNN
F 2 "" H 1400 3350 50  0001 C CNN
F 3 "" H 1400 3350 50  0001 C CNN
	1    1400 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6033A063
P 2200 2950
AR Path="/6033A063" Ref="R?"  Part="1" 
AR Path="/61541A83/6033A063" Ref="R15"  Part="1" 
F 0 "R15" V 1993 2950 50  0000 C CNN
F 1 "10k" V 2084 2950 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2130 2950 50  0001 C CNN
F 3 "~" H 2200 2950 50  0001 C CNN
	1    2200 2950
	0    -1   1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 6033A069
P 2400 3150
AR Path="/6033A069" Ref="C?"  Part="1" 
AR Path="/61541A83/6033A069" Ref="C2"  Part="1" 
F 0 "C2" H 2515 3196 50  0000 L CNN
F 1 "10nF(HV)" H 2515 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.33x1.80mm_HandSolder" H 2438 3000 50  0001 C CNN
F 3 "~" H 2400 3150 50  0001 C CNN
	1    2400 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2950 2400 3000
Wire Wire Line
	1400 3150 1400 3300
Wire Wire Line
	2400 3300 1400 3300
Text Label 1600 2950 0    50   ~ 0
PStop_in
Wire Wire Line
	2400 2950 2350 2950
Connection ~ 2400 2950
Wire Wire Line
	2400 2950 2800 2950
Wire Wire Line
	1600 2950 2050 2950
$Comp
L Device:Jumper JP?
U 1 1 6033A077
P 1750 2600
AR Path="/6033A077" Ref="JP?"  Part="1" 
AR Path="/61541A83/6033A077" Ref="JP3"  Part="1" 
F 0 "JP3" H 1750 2375 50  0000 C BNN
F 1 "J_BackBias" H 1750 2466 50  0000 C BNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1750 2600 50  0001 C CNN
F 3 "~" H 1750 2600 50  0001 C CNN
	1    1750 2600
	1    0    0    1   
$EndComp
Wire Wire Line
	900  2600 1450 2600
Text Label 900  2600 0    50   ~ 0
BackBias_in
Wire Wire Line
	2050 2600 2050 2950
Connection ~ 2050 2950
Wire Wire Line
	1400 3350 1400 3300
Connection ~ 1400 3300
Text HLabel 2800 2950 2    50   Output ~ 0
SUBSTRATE
Text HLabel 5900 1750 0    50   Input ~ 0
VDDA
$EndSCHEMATC
